
package fr.univ.savoie.isc;

public class Quarantine {

    public Quarantine(char[] subjects) {

        // H : Healthy
        // F : Fever
        // D : Diabetes
        // T : Tuberculosis

        throw new UnsupportedOperationException();
    }

    public Quarantine withAspirin() {
        // cure Fever
        throw new UnsupportedOperationException();
    }

    public Quarantine withAntibiotic() {
        // cure Tuberculosis
        // healthy people catch Fever if mixed with withInsulin.
        throw new UnsupportedOperationException();
    }

    public Quarantine withInsulin() {
        // prevent diabetic subject from dying, does not cure Diabetes,
        // healthy people catch Fever if mixed with withAntibiotic.
        throw new UnsupportedOperationException();
    }

    public Quarantine wait40Days(){
        throw new UnsupportedOperationException();
    }

    // "F:1 H:2 D:0 T:1 X:3"
    // F:1 where F means Fever and 1 the number of subject suffering from Fever
    // X => Dead
    public String checkHealth() {
        throw new UnsupportedOperationException();
    }

}