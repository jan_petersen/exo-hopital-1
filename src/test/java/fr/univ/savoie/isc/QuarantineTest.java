
package fr.univ.savoie.isc;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class QuarantineTest {

    @Test
    public void whenNoTimeElapsed() throws Exception {
        Quarantine quarantine = new Quarantine("FHDDDHT".toCharArray());
        assertEquals("F:1 H:2 D:3 T:1 X:0", quarantine.checkHealth());
    }


    @Test
    public void whenEveryoneIsHealthy() throws Exception {
        Quarantine quarantine = new Quarantine("HHHHHH".toCharArray());
        quarantine.wait40Days();
        assertEquals("F:0 H:6 D:0 T:0 X:0", quarantine.checkHealth());
    }


    @Test
    public void whenNoTreatment() throws Exception {
        Quarantine quarantine = new Quarantine("FHDDDHT".toCharArray());
        quarantine.wait40Days();
        assertEquals("F:1 H:2 D:0 T:1 X:3", quarantine.checkHealth());
    }


    @Test
    public void withAspirin() throws Exception {
        Quarantine quarantine = new Quarantine("FHDDDHT".toCharArray());
        quarantine.withAspirin();
        quarantine.wait40Days();
        assertEquals("F:0 H:3 D:0 T:1 X:3", quarantine.checkHealth());
    }

    @Test
    public void withAntibiotic() throws Exception {
        Quarantine quarantine = new Quarantine("FHDDDHT".toCharArray());
        quarantine.withAntibiotic();
        quarantine.wait40Days();
        assertEquals("F:1 H:3 D:0 T:0 X:3", quarantine.checkHealth());
    }

    @Test
    public void withInsulin() throws Exception {
        Quarantine quarantine = new Quarantine("FHDDDHT".toCharArray());
        quarantine.withInsulin();
        quarantine.wait40Days();
        assertEquals("F:1 H:2 D:3 T:1 X:0", quarantine.checkHealth());
    }

    @Test
    public void withAntibioticAndInsulin() throws Exception {
        Quarantine quarantine = new Quarantine("FHDDDHT".toCharArray());
        quarantine.withAntibiotic();
        quarantine.withInsulin();
        quarantine.wait40Days();
        assertEquals("F:3 H:1 D:3 T:0 X:0", quarantine.checkHealth());
    }


/**
 * Introducing paracetamol : heals fever, kills subject if mixed with aspirin (introducing mixed effect for all subject)
 */

//    @Test
//    public void withParacetamol() throws Exception {
//        Quarantine quarantine = new Quarantine("FHDDDHT".toCharArray());
//        quarantine.withParacetamol();
//        quarantine.wait40Days();
//        assertEquals("F:0 H:3 D:0 T:1 X:3", quarantine.checkHealth());
//    }

//    @Test //See ideas.txt
//    public void withParacetamolAndAspirin() throws Exception {
//        Quarantine quarantine = new Quarantine("FHDDDHT".toCharArray());
//        quarantine.withParacetamol();
//        quarantine.withAspirin();
//        quarantine.wait40Days();
//        assertEquals("F:0 H:0 D:0 T:0 X:7", quarantine.checkHealth());
//    }

/**
 * introduce hemophilia : not curable, aspirin kills subject. (introducing illness specific effect for aspirin)
 */

}